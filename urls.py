from django.conf.urls.defaults import *
from cookforme.app.views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^index/$', index),
    url(r'^$', index),
    url(r'^cautaret/$', cautaret),
	url(r'^cautaret/(\w+)/$', cautaret),
	url(r'^descr/(\w+)/$', descr),
	url(r'^descr/$', descr),
	url(r'^admin/', include(admin.site.urls)),
    url(r'^pascupas/(\w+)/$', pascupas),
	url(r'^pascupas/$', pascupas),
)
