from django.contrib import admin
from cookforme.app.models import Ingredient, Categorie, Tag, Reteta, Dificultate, Tara

class RetetaAdmin(admin.ModelAdmin):
	list_display = ('nume', 'descriere', 'foto')
	search_fields = ('nume', 'descriere')
	ordering = ('-nume',)
	#filter_horizontal = ('ingrediente','tags',)


admin.site.register(Ingredient)
admin.site.register(Categorie)
admin.site.register(Tag)
admin.site.register(Dificultate)
admin.site.register(Tara)
admin.site.register(Reteta, RetetaAdmin)