# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render
from cookforme.app.models import *
from django.core.exceptions import *
import re


def index(request):
    tags = Tag.objects.all()
    noTags = []
    for t in tags:
		k = t.reteta_set.all().count()
		noTags.append(k)
    return render(request, 'index.html', {'tags': tags, 'noTags': noTags})

def cautaret(request):
	if not 'find' in request.GET:
		find = 'nimic.'
	else:
		find = request.GET['find']
	if find == '':		#utilizatorul nu introduce nimic in camp
		retete2 = Reteta.objects.all().order_by('dificultate')
	else:
		if not 'number' in request.GET:
			procent = 60
		else:
			procent = request.GET['number']
		if procent == '':
			procent = 1
		procent = float(procent)/100		#ce are mai putin de acest procent nu va fi afisat
		retete = []
		taglist = re.sub(r'[.!,;?]', ' ', find).split() #imparte sirul de intrare in cuvinte
		for tag in taglist:
			try:
				tags = Tag.objects.get(nume = tag)	#interogare BD pentru tag
			except ObjectDoesNotExist:
				tags=''
			try:
				#tags = 'rosii'
				retete_db = tags.reteta_set.all().order_by('dificultate') #interogare pentru retetele specifice tagului curent
			except AttributeError:
				retete_db = ''
				#adauga retetele curente la retete
			for r in retete_db:
				if not r in retete:
					retete.append(r)

		count = list(0 for i in retete)
		k=0
		for r in retete:
			for t in taglist:
				ok = r.tags.filter(nume=t)
				if len(ok) != 0:
					count[k] = count[k] + 1		#numara cate taguri introduse de utilizator are fiecare reteta din rezultatul initial
			k=k+1
		retete2 = [retete[i] for i in range(0, len(count)) if count[i] / float (len(taglist)) >= procent]

	#return HttpResponse(procent)
	return render(request, 'cautaret.html', {'find': find, 'retete': retete2})

def descr(request):
    if not 'id' in request.GET:
		reteta = Reteta.objects.order_by('?')[1]
    else:
		id = request.GET['id']
		reteta = Reteta.objects.get(id=id)
    ingrediente = reteta.ingrediente.all()
    tags = reteta.tags.all()
    return render(request, 'descr.html', {'reteta': reteta, 'ingrediente':ingrediente, 'tags':tags})

def pascupas(request):
    if not 'id' in request.GET:
		return HttpResponse('Incerci sa ma testezi?')
    else:
		id = request.GET['id']
		reteta = Reteta.objects.get(id=id)
		pas = request.GET['pas']
		pas = str(int(pas) + 1)
		id = str(id)
		nextURL = "http://tb.pythonanywhere.com/pascupas/?id=" + id + "&pas=" + pas
		sentenceEnders = re.compile('[.!?]')
		sentenceList = sentenceEnders.split(reteta.pascupas)
		sentence = sentenceList[int(pas)-2]
		noTotal = str(len(sentenceList))
		pas0 = int(pas) - 1
		max = len(sentenceList) - 1
		#return HttpResponse(sentenceList)
		return render(request, 'pascupas.html', {'pas': pas, 'pas0':pas0, 'nextURL':nextURL, 'noTotal':noTotal, 'id':id, 'sentence':sentence, 'max':max})