from django.db import models

# Create your models here.

class Ingredient(models.Model):
    nume = models.CharField(max_length=100)
    def __unicode__(self):
		return self.nume

class Categorie(models.Model):
	nume = models.CharField(max_length=100)
	def __unicode__(self):
		return self.nume

class Tag(models.Model):
	nume = models.CharField(max_length=100)
	def __unicode__(self):
		return self.nume

class Dificultate(models.Model):
	nume = models.CharField(max_length=100)
	def __unicode__(self):
		return self.nume

class Tara(models.Model):
	nume = models.CharField(max_length=100)
	def __unicode__(self):
		return self.nume

class Reteta(models.Model):
	nume = models.CharField(max_length=100)
	tara = models.ForeignKey(Tara)
	dificultate = models.ForeignKey(Dificultate)
	descriere = models.TextField()
	categorie = models.ForeignKey(Categorie)
	durata = models.IntegerField()
	foto = models.URLField(blank=True)
	ingrediente = models.ManyToManyField(Ingredient)
	cantitati = models.TextField(blank=True)
	pascupas = models.TextField()
	tags = models.ManyToManyField(Tag)
	def __unicode__(self):
		return u'%s %s' % (self.nume, self.foto)